import Moment from 'moment';

function todayDate(number){
  var formatted = Moment().format("DD-MM-YYYY");
  var now = Moment(formatted, "DD-MM-YYYY");

  return now;
}

function todayDayOfWeek(){
  return Moment().weekday();
}

function dowName(index){
  var array = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  return array[index];
}

function nearestDate(todayDate, weekday){
  for(let i=1; i<8; i++){
    let td = Moment(todayDate, "DD-MM-YYYY");
    let appendedDate = td.add(i, 'day');
    if(appendedDate.weekday() == weekday){
      return appendedDate;
    }
  }
}

function sameDowArray(date, dayOfWeek){
  var result = [];
  var nearest = nearestDate(date, dayOfWeek);
  result.push(Moment(nearest,"DD-MM-YYYY").subtract(14, 'day'));
  result.push(Moment(nearest,"DD-MM-YYYY").subtract(7, 'day'));
  result.push(nearest);
  result.push(Moment(nearest,"DD-MM-YYYY").add(7, 'day'));
  result.push(Moment(nearest,"DD-MM-YYYY").add(14, 'day'));
  return result;
}

export { sameDowArray, dowName, todayDate, todayDayOfWeek, nearestDate }
