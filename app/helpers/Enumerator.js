import {
  ListView,
} from 'react-native';

function createDataSource(list){
  var data_list = [];
  for(var index in list){
    data_list.push(list[index]);
  }

  var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
  return ds.cloneWithRows(data_list);
}

export { createDataSource };
