function prependZero(number){
  if(parseInt(number) >= 10){
    return `${number}`;
  }

  return `0${number}`;
}

export { prependZero }
