import React from 'react';
import {
  ToastAndroid,
  Picker
} from 'react-native';

import {
  Button,
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Text
} from 'native-base';

import Schedule from '../../models/Schedule';
import DatePicker from 'react-native-datepicker'

import Realm from 'realm';
import ScheduleForm from './Form';

export default class ScheduleEdit extends React.Component {
  static navigationOptions = {
    title: 'Schedule Edit',
  };

  constructor(props){
    super(props);

    var id = this.props.navigation.state.params.id;
    var current = Schedule.find(id);

    this.state = current.hash();
  }

  render() {
    return (
      <ScheduleForm
        state={this.state}
        onTimeChange={this.onTimeChange.bind(this)}
        onDowChange={this.onDowChange.bind(this)}
        onButtonClick={this.onButtonClick.bind(this)}
      />
    );
  }

  onTimeChange(time){
    this.setState({ time: time });
  }

  onDowChange(value,index){
    this.setState({ day_of_week: value });
  }

  onButtonClick(){
    const { goBack } = this.props.navigation;

    Schedule.update({
      id: this.state.id,
      time: this.state.time,
      day_of_week: parseInt(this.state.day_of_week)
    });
    ToastAndroid.show('Schedule updated!', ToastAndroid.SHORT);

    goBack();
  }
}
