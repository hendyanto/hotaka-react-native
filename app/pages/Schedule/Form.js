import React from 'react';
import {
  ToastAndroid,
  Picker
} from 'react-native';

import {
  Button,
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Text
} from 'native-base';


import Schedule from '../../models/Schedule';
import DatePicker from 'react-native-datepicker'

import Realm from 'realm';

export default class ScheduleNew extends React.Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      <Container>
        <Content>
          <Form>
          <Label>Date</Label>
          <DatePicker
            style={{width: 200}}
            date={this.props.state.time}
            mode="time"
            placeholder="select time"
            format="HH:mm"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0
              },
              dateInput: {
                marginLeft: 36
              }
            }}
            onDateChange={this.props.onTimeChange}
          />
          <Picker
            selectedValue={`${this.props.state.day_of_week}`}
            onValueChange={this.props.onDowChange}>

            <Picker.Item label="Sunday" value="0" />
            <Picker.Item label="Monday" value="1" />
            <Picker.Item label="Tuesday" value="2" />
            <Picker.Item label="Wednesday" value="3" />
            <Picker.Item label="Thursday" value="4" />
            <Picker.Item label="Friday" value="5" />
            <Picker.Item label="Saturday" value="6" />

          </Picker>
          </Form>
          <Button block onPress={this.props.onButtonClick}>
            <Text>Submit</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
