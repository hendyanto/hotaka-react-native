import React from 'react';
import {
  ListView,
} from 'react-native';

import {
  Button,
  Container,
  Body,
  List,
  ListItem,
  Right,
  Text
} from 'native-base';

import Schedule from '../../models/Schedule';

import Realm from 'realm';

import { createDataSource } from '../../helpers/Enumerator';
import { dowName } from '../../helpers/Date';

export default class ScheduleList extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { navigate, state } = navigation;
    const { student } = state.params;
    return {
      title: `${student.name()} Schedule List`,
      headerRight: (
        <Button small transparent onPress={() => {
          navigate('ScheduleNew', { student_id: student.id() });
        }}>
          <Text>New +</Text>
        </Button>
      )
    };
  };

  constructor(props){
    super(props);
    this.refreshList();

    Schedule.onChange(() => {
      this.forceUpdate();
      this.refreshList();
    });
  }

  refreshList(){
    var data_list = [];
    var student_id = this.props.navigation.state.params.student.id();
    var list = Schedule.byStudent(student_id);

    this.state = {
      student_id: student_id,
      dataSource: createDataSource(list)
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <List>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>
            <ListItem>
              <Body>
                <Text>{dowName(rowData.day_of_week())} {rowData.time()}</Text>
              </Body>
              <Right>
                <Button small dark bordered onPress={() => {
                  navigate('ScheduleEdit', { id: rowData.id() });
                }}>
                  <Text style={{ color: 'black' }}>Edit</Text>
                </Button>
              </Right>
            </ListItem>
          }
        />
      </List>
    );
  }
}
