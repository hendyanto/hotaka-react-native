import React from 'react';
import {
  ToastAndroid,
  Picker
} from 'react-native';

import {
  Button,
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Text
} from 'native-base';


import Schedule from '../../models/Schedule';
import DatePicker from 'react-native-datepicker'

import Realm from 'realm';
import ScheduleForm from './Form';

export default class ScheduleNew extends React.Component {
  static navigationOptions = {
    title: 'Schedule New',
  };

  constructor(props){
    super(props);
    var schedule = new Schedule();
    var student_id = this.props.navigation.state.params.student_id;

    this.state = Object.assign(schedule.hash(), { student_id: student_id });
  }

  render() {
    return (
      <ScheduleForm
        state={this.state}
        onTimeChange={this.onTimeChange.bind(this)}
        onDowChange={this.onDowChange.bind(this)}
        onButtonClick={this.onButtonClick.bind(this)}
      />
    );
  }

  onTimeChange(time){
    this.setState({ time: time });
  }

  onDowChange(value,index){
    this.setState({ day_of_week: value });
  }

  onButtonClick(){
    const { goBack } = this.props.navigation;

    Schedule.insert({
      student_id: parseInt(this.state.student_id),
      reason: this.state.reason,
      time: this.state.time,
      day_of_week: parseInt(this.state.day_of_week)
    });

    ToastAndroid.show('New schedule added!', ToastAndroid.SHORT);

    goBack();
  }
}
