import React from 'react';
import { View, ScrollView } from 'react-native';
import {
  Container,
  Content,
} from 'native-base';

import ScheduleList from './Home/ScheduleList';
import DebtList from './Home/DebtList';

export default class Home extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };

  constructor(props){
    super(props);
  }

  render() {
    return (
      <Container>
        <Content>
          <ScheduleList navigation={this.props.navigation} />
          <DebtList navigation={this.props.navigation} />
        </Content>
      </Container>
    );
  }
}
