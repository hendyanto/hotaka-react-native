import React from 'react';
import {
  ListView,
} from 'react-native';
import {
  Button,
  Container,
  Body,
  List,
  ListItem,
  Right,
  Text
} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';

import Debt from '../../models/Debt';
import { createDataSource } from '../../helpers/Enumerator';

export default class DebtList extends React.Component {

  constructor(props){
    super(props);

    Debt.onChange(() => {
      this.forceUpdate();
      this.refreshList();
    });

    this.refreshList();
  }

  refreshList(){
    var list = Debt.greaterThanToday().concat(Debt.notScheduled());

    this.state = {
      dataSource: createDataSource(list)
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <List>
        <ListItem itemDivider><Text>Outstanding Debts</Text></ListItem>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>

              <ListItem button onPress={() => {
                navigate('DebtEdit', { id: rowData.id() });
              }}>
                <Body>
                    <Text style={{fontWeight: "bold"}}>
                      {rowData.student().name()}
                    </Text>
                    <Text>
                      { `${ rowData.formattedDate() } to` }
                    </Text>
                    <Text>
                      { rowData.formattedPaymentDate() }
                    </Text>
                </Body>
                <Right>
                  <Icon name="angle-right" />
                </Right>
              </ListItem>
          }
        />
      </List>
    );
  }
}
