import React from 'react';
import {
  ListView,
} from 'react-native';
import {
  Button,
  Container,
  Body,
  List,
  ListItem,
  Right,
  Text
} from 'native-base';

import { createDataSource } from '../../helpers/Enumerator';

import TodaySchedule from '../../models/TodaySchedule';
import Schedule from '../../models/Schedule';

export default class ScheduleList extends React.Component {

  constructor(props){
    super(props);

    Schedule.onChange(() => {
      this.forceUpdate();
      this.refreshList();
    });

    this.refreshList();
  }

  refreshList(){
    var td = new TodaySchedule();
    var list = td.get();

    this.state = {
      dataSource: createDataSource(list)
    };
  }

  render() {
    return (
      <List>
        <ListItem itemDivider><Text>Today Schedules</Text></ListItem>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>

              <ListItem>
                <Body>
                    <Text style={{fontWeight: "bold"}}>
                      {rowData.studentName()} - {rowData.time()}
                    </Text>
                    <Text note>{(rowData.isDebt() ? "Debt payment" : "Regular Schedule")}</Text>
                    <Text note>{(rowData.hasDebt() ? `Moved to ${rowData.debt().formattedPaymentDate()}` : "")}</Text>
                </Body>
              </ListItem>
          }
        />
      </List>
    );
  }
}
