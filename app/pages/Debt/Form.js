import React from 'react';
import Moment from 'moment';
import { todayDate, sameDowArray } from '../../helpers/Date';

import {
  ToastAndroid
} from 'react-native';

import {
  Button,
  Container,
  Content,
  Picker,
  Form,
  Item,
  Label,
  Input,
  Text
} from 'native-base';

import Debt from '../../models/Debt';
import Schedule from '../../models/Schedule';
import DatePicker from 'react-native-datepicker'

import Realm from 'realm';

export default class DebtForm extends React.Component {

  constructor(props){
    super(props);
  }

  render() {
    let dateItems = <Item label="You need to set schedule for this student first" value="" />
    let student = this.props.state.student;
    let firstSchedule = student.firstSchedule();
    let pivot = todayDate();
    if(typeof this.props.state.date != "undefined" && this.props.state.date != ""){
      pivot = Moment(this.props.state.date, "DD-MM-YYYY HH:mm");
    }

    if(typeof firstSchedule != "undefined"){
      let sameDow = sameDowArray(pivot, firstSchedule.day_of_week());
      dateItems = sameDow.map((item) => {
        return <Item
        label={`${item.format("DD-MM-YYYY")} ${firstSchedule.time()} (${item.fromNow()})`}
        value={`${item.format('DD-MM-YYYY')} ${firstSchedule.time()}`} />
      });
    }
    return (
      <Container>
        <Content>
          <Form>
          <Label>Date</Label>
          <Picker
            supportedOrientations={['portrait','landscape']}
            iosHeader="Select one"
            headerBackButtonText="Go Back"
            mode="dropdown"
            selectedValue={this.props.state.date}
            onValueChange={this.props.onDateChange}>
            { dateItems }
         </Picker>

          <Label>Payment Date</Label>
          <DatePicker
            style={{width: 200}}
            date={this.props.state.payment_date}
            mode="datetime"
            placeholder="select date"
            format="DD-MM-YYYYTHH:mm:ssZ"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0
              },
              dateInput: {
                marginLeft: 36
              }
            }}
            onDateChange={this.props.onPaymentDateChange}
          />
            <Item floatingLabel>
              <Label>Reason</Label>
              <Input
                onChangeText={this.props.onReasonChange}
                value={this.props.state.reason}
              />
            </Item>
          </Form>
          <Button block onPress={this.props.onButtonClick}>
            <Text>Submit</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

