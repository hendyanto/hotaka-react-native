import React from 'react';
import {
  ListView,
} from 'react-native';

import {
  Button,
  Container,
  Card,
  CardItem,
  Body,
  List,
  ListItem,
  Right,
  Text
} from 'native-base';

import { createDataSource } from '../../helpers/Enumerator';

import Debt from '../../models/Debt';
import DebtWidget from './Widget';

import Realm from 'realm';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class DebtList extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { navigate, state } = navigation;
    const { student } = state.params;
    return {
      title: `${student.name()} Debt List`,
      headerRight: (
        <Button small transparent onPress={() => {
          navigate('DebtNew', { student_id: student.id() });
        }}>
          <Text>New +</Text>
        </Button>
      )
    };
  };

  constructor(props){
    super(props);
  }

  componentWillMount(){
    Debt.onChange(() => {
      this.forceUpdate();
      this.refreshList();
    });
    this.refreshList();
  }

  refreshList(){
    var student_id = this.props.navigation.state.params.student.id();
    var list = Debt.byStudent(student_id);

    this.state = {
      student_id: student_id,
      dataSource: createDataSource(list)
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <List>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>
            <DebtWidget navigation={this.props.navigation} debt={rowData} />
          }
        />
      </List>
    );
  }
}
