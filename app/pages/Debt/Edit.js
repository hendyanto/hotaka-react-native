import React from 'react';
import {
  ToastAndroid
} from 'react-native';

import {
  Button,
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Text
} from 'native-base';

import Debt from '../../models/Debt';
import DatePicker from 'react-native-datepicker'

import Realm from 'realm';
import DebtForm from './Form';

export default class DebtEdit extends React.Component {
  static navigationOptions = {
    title: 'Debt Edit',
  };

  constructor(props){
    super(props);

    var id = this.props.navigation.state.params.id;
    var current = Debt.find(id);

    this.state = Object.assign(current.hash(),{
      student: current.student()
    });
  }

  render() {
    return (
      <DebtForm
        state = {this.state}
        onDateChange = {this.onDateChange.bind(this)}
        onPaymentDateChange = {this.onPaymentDateChange.bind(this)}
        onReasonChange = {this.onReasonChange.bind(this)}
        onButtonClick = {this.onButtonClick.bind(this)}
      />
    );
  }

  onDateChange(date){
    this.setState({ date: date })
  }

  onPaymentDateChange(payment_date){
    this.setState({ payment_date: payment_date })
  }

  onReasonChange(text){
    this.setState({ reason: text });
  }

  onButtonClick(){
    const { goBack } = this.props.navigation;

    Debt.update({
      id: this.state.id,
      reason: String(this.state.reason),
      date: String(this.state.date),
      payment_date: String(this.state.payment_date),
    });
    ToastAndroid.show('Debt updated!', ToastAndroid.SHORT);

    goBack();
  }
}
