import React from 'react';
import {
  ToastAndroid
} from 'react-native';

import {
  Button,
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Text
} from 'native-base';

import Debt from '../../models/Debt';
import Student from '../../models/Student';
import DatePicker from 'react-native-datepicker'

import Realm from 'realm';
import DebtForm from './Form';

export default class DebtNew extends React.Component {
  static navigationOptions = {
    title: 'Debt New',
  };

  constructor(props){
    super(props);

    var debt = new Debt();

    var student_id = this.props.navigation.state.params.student_id;

    this.state = Object.assign(debt.hash(), {
      student_id: student_id,
      student: Student.find(student_id)
    });
  }

  render() {
    return (
      <DebtForm
        state = {this.state}
        onDateChange = {this.onDateChange.bind(this)}
        onPaymentDateChange = {this.onPaymentDateChange.bind(this)}
        onReasonChange = {this.onReasonChange.bind(this)}
        onButtonClick = {this.onButtonClick.bind(this)}
      />
    );
  }

  onDateChange(date){
    this.setState({ date: date })
  }

  onPaymentDateChange(payment_date){
    this.setState({ payment_date: payment_date })
  }

  onReasonChange(text){
    this.setState({ reason: text });
  }

  onButtonClick(){
    const { goBack } = this.props.navigation;

    Debt.insert({
      student_id: parseInt(this.state.student_id),
      reason: String(this.state.reason),
      date: String(this.state.date),
      payment_date: String(this.state.payment_date),
    });
    ToastAndroid.show('New debt added!', ToastAndroid.SHORT);

    goBack();
  }
}
