import React from 'react';

import {
  Button,
  Container,
  Card,
  CardItem,
  Body,
  List,
  ListItem,
  Right,
  Text
} from 'native-base';

import Debt from '../../models/Debt';

import Realm from 'realm';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class DebtWidget extends React.Component {

  constructor(props){
    super(props);
    this.debt = this.props.debt;
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Card style={{ flex: 0 }}>
        <CardItem header>
          <Text>Missed at: {this.debt.date()}</Text>
        </CardItem>
        <CardItem button onPress={() => {
              navigate('DebtEdit', { id: this.debt.id() });
            }}>
          <Body>
            <Text>Moved to:</Text>
            <Text>{this.debt.formattedPaymentDate()}</Text>
          </Body>
          <Right>
            <Icon name="angle-right" />
          </Right>
        </CardItem>
        <CardItem header>
          <Text note>Reason: {this.debt.reason()}</Text>
        </CardItem>
      </Card>
    );
  }
}
