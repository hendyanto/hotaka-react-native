import React from 'react';
import {
  ToastAndroid
} from 'react-native';

import {
  Button,
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Text
} from 'native-base';

import Student from '../../models/Student';

import Realm from 'realm';

export default class StudentNew extends React.Component {
  static navigationOptions = {
    title: 'Student New',
  };

  constructor(){
    super();
    this.data = new Student();
    var data_list = [];
    this.state = this.data.hash();
  }

  render() {
    return (
      <Container>
        <Content>
          <Form>
            <Item floatingLabel>
              <Label>Name</Label>
              <Input
                onChangeText={(text) => this.setState({name: text})}
                value={this.state.name}
              />
            </Item>
            <Item floatingLabel>
              <Label>Grade</Label>
              <Input
                onChangeText={(text) => this.setState({grade: text})}
                value={this.state.grade}
              />
            </Item>
          </Form>
          <Button block onPress={this.onButtonClick.bind(this)}>
            <Text>Add</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  onButtonClick(){
    const { goBack } = this.props.navigation;

    Student.insert({ name: this.state.name, grade: this.state.grade });
    ToastAndroid.show('New student added!', ToastAndroid.SHORT);

    goBack();
  }
}
