import React from 'react';
import {
  ScrollView,
} from 'react-native';

import {
  Button,
  Body,
  H1,
  Container,
  Right,
  Card,
  CardItem,
  Text
} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';

import Student from '../../models/Student';
import Debt from '../../models/Debt';
import Schedule from '../../models/Schedule';

import Realm from 'realm';
import DebtList from '../Debt/List';

import { TabNavigator, StackNavigator } from 'react-navigation';


export default class StudentShow extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { navigate, state } = navigation;
    const { student } = state.params;
    return {
      title: `${student.name()} (Grade ${student.grade()})`,
      headerRight: (
        <Button small transparent onPress={() => {
          navigate('StudentEdit', { student: student });
        }}>
          <Text>Edit</Text>
        </Button>
      )
    };
  };

  constructor(props){
    super(props);
    this.data = new Student();
    var debt_data = new Debt();
    var schedule_data = new Schedule();
    var data_list = [];
    var id = this.props.navigation.state.params.student.id();

    var current = this.props.navigation.state.params.student;

    // var debtCount = debt_data.byStudent(current.id).length;
    // var scheduleCount = schedule_data.byStudent(current.id).length;

    // debt_data.onChange(() => {
    //   this.setState({ debtCount: debt_data.byStudent(current.id).length });
    //   this.forceUpdate();
    // });
    //
    // schedule_data.onChange(() => {
    //   this.setState({ scheduleCount: schedule_data.byStudent(current.id).length });
    //   this.forceUpdate();
    // });

    this.state = {
      student: current,
      // scheduleCount: scheduleCount,
      // debtCount: debtCount
    };
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <Container>
        <ScrollView>
          <Card>
            <CardItem button onPress={() => {
              navigate('DebtList', { student: this.state.student });
            }}>
              <Body>
                  <H1>{this.state.debtCount} Debts</H1>
              </Body>
              <Right>
                <Icon name="angle-right" />
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem button onPress={() => {
              navigate('ScheduleList', { student: this.state.student });
            }}>
              <Body>
                  <H1>{this.state.scheduleCount} Schedules</H1>
              </Body>
              <Right>
                <Icon name="angle-right" />
              </Right>
            </CardItem>
          </Card>
        </ScrollView>
      </Container>
    );
  }
}
