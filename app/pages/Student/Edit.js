import React from 'react';
import {
  TextInput,
  ListView,
  ScrollView,
  View,
  ToastAndroid
} from 'react-native';

import {
  Button,
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Text
} from 'native-base';

import Student from '../../models/Student';

import Realm from 'realm';

export default class StudentEdit extends React.Component {
  static navigationOptions = {
    title: 'Student Edit',
  };

  constructor(props){
    super(props);
    var data_list = [];
    var id = this.props.navigation.state.params.student.id();

    var current = Student.find(id);

    this.state = current.hash();
  }

  render() {
    return (
      <Container>
        <Content>
          <Form>
            <Item floatingLabel>
              <Label>Name</Label>
              <Input
                onChangeText={(text) => this.setState({name: text})}
                value={this.state.name}
              />
            </Item>
            <Item floatingLabel>
              <Label>Grade</Label>
              <Input
                onChangeText={(text) => this.setState({grade: text})}
                value={this.state.grade}
              />
            </Item>
          </Form>
          <Button block onPress={this.onButtonClick.bind(this)}>
            <Text>Update</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  onButtonClick(){
    const { goBack } = this.props.navigation;

    Student.update({ id: this.state.id, name: this.state.name, grade: this.state.grade });
    ToastAndroid.show('Student updated!', ToastAndroid.SHORT);

    goBack();
  }
}
