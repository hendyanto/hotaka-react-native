import React from 'react';
import {
  ListView,
} from 'react-native';

import { createDataSource } from '../../helpers/Enumerator';

import {
  Body,
  Card,
  Button,
  CardItem,
  Container,
  H1,
  Right,
  Text
} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';

import Student from '../../models/Student';

export default class StudentList extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { navigate } = navigation;
    return {
      title: 'My Students',
      headerRight: (
        <Button small transparent onPress={() => {
          navigate('StudentNew');
        }}>
          <Text>Add </Text>
          <Icon name="user-plus" />
        </Button>
      )
    };
  };

  constructor(){
    super();

    Student.onChange(() => {
      this.forceUpdate();
      this.refreshList();
    });

    this.refreshList();
  }

  refreshList(data_list){
    var list = Student.getAll();
    this.state = {
      dataSource: createDataSource(list)
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
        <Container>
          <ListView
            dataSource={this.state.dataSource}
            renderRow={(rowData) =>

              <Card style={{ flex: 0 }}>
                <CardItem button onPress={() => {
                  navigate('StudentShow', { student: rowData });
                }}>
                  <Body>
                      <H1>{rowData.name()}</H1>
                      <Text>Grade {rowData.grade()}</Text>
                  </Body>
                  <Right>
                    <Icon name="angle-right" />
                  </Right>
                </CardItem>
              </Card>
            }
          />
        </Container>
    );
  }
}

