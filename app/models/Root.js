import Realm from 'realm';

export class Student extends Realm.Object{}
Student.schema = {
  name: 'Student',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    grade: 'string'
  }
};

export class Debt extends Realm.Object{}
Debt.schema = {
  name: 'Debt',
  primaryKey: 'id',
  properties: {
    id: 'int',
    date: 'string',
    payment_date: 'string',
    reason: 'string',
    student_id: 'int',
  }
};

export class User extends Realm.Object{}
User.schema = {
  name: 'User',
  primaryKey: 'id',
  properties: {
    id: 'int',
    email: 'string',
    password: 'string'
  }
};

export class Schedule extends Realm.Object{}
Schedule.schema = {
  name: 'Schedule',
  primaryKey: 'id',
  properties: {
    id: 'int',
    student_id: 'int',
    day_of_week: 'int',
    time: 'string'
  }
};

var schemas = [
  {
    schemaVersion: 1,
    migration: function(){
    },
    schema: [Student, Debt]
  },
  {
    schemaVersion: 2,
    migration: function(){},
    schema: [Student, Debt, User, Schedule]
  }
];

var nextSchemaIndex = Realm.schemaVersion(Realm.defaultPath);
while (nextSchemaIndex < schemas.length) {
  var migratedRealm = new Realm({ ...schemas[nextSchemaIndex++] });
  migratedRealm.close();
}

export default new Realm(schemas[schemas.length-1]);
