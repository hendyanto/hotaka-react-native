import Model from './Model';
import Student from './Student';
import { todayDate } from '../helpers/Date';

import Moment from 'moment';

export default class Debt extends Model {
  constructor(item){
    super(item);
  }

  static tableName(){
    return 'Debt';
  }

  static byStudent(student_id){
    return this.where('student_id', student_id);
  }

  static debtWithTodayPaymentDate(){
    return this.byPaymentDate(todayDate());
  }

  static byPaymentDate(date){
    return this.whereContains('payment_date', date);
  }

  static byDate(date){
    return this.whereContains('date', date);
  }

  static byPaymentDate(date){
    var result = [];
    var all = this.getAll();
    all.forEach(function(item){
      let payment_date = item.getPaymentDate();
      if(payment_date.isSame(date)){
        result.push(item);
      }
    });

    return result;
  }

  static greaterThanToday(){
    return this.greaterThanDate(todayDate());
  }

  static notScheduled(){
    return this.where('payment_date','""');
  }

  static greaterThanDate(date) {
    var all = this.getAll();
    var today = todayDate().startOf('day');
    var results = [];
    all.map((item) => {
      let date = new Moment(item.payment_date(),"DD-MM-YYYY HH:mm");
      if(date.isAfter(today)){
        results.push(item);
      }
    });

    return results;
  }

  hash(){
    return {
      id: this.instance().id,
      date: this.instance().date || "",
      payment_date: this.instance().payment_date || "",
      reason: this.instance().reason || "",
      student_id: this.instance().student_id,
    };
  }

  date(){
    return this.instance().date;
  }

  formattedPaymentDate(){
    if(this.payment_date() == ""){
      return "To be announced";
    }
    return Moment(this.payment_date(),"DD-MM-YYYYTHH:mm:ssZ").format("DD-MM-YYYY HH:mm");
  }

  formattedDate(){
    if(this.date() == ""){
      return "Undefined";
    }
    return Moment(this.date(),"DD-MM-YYYYTHH:mm:ssZ").format("DD-MM-YYYY HH:mm");
  }

  payment_date(){
    return this.instance().payment_date;
  }

  reason(){
    return this.instance().reason;
  }

  student_id(){
    return this.instance().student_id;
  }

  student(){
    return Student.find(this.student_id());
  }

  id(){
    return this.instance().id;
  }

  getPaymentDate(){
    var payment_date = Moment(this.payment_date(),"DD-MM-YYYYTHH:mm:ssZ");
    return Moment(payment_date.format("DD-MM-YYYY"),"DD-MM-YYYY");
  }

  getPaymentTime(){
    var payment_date = Moment(this.payment_date(),"DD-MM-YYYYTHH:mm:ssZ");
    return payment_date.format("HH:mm")
  }

  getTime(){
    var date = Moment(this.date(),"DD-MM-YYYYTHH:mm:ssZ");
    return date.format("HH:mm")
  }

}
