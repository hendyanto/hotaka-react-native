import Root from './Root';
import Model from './Model';

export default class User extends Model {
  constructor(){
    super();
    this._name = 'User';

    this._data = Root;
  }
}
