import Student from './Student';
import Model from './Model';

export default class Schedule extends Model {
  constructor(item){
    super(item);
  }

  hash(){
    return {
      id: this.instance().id,
      day_of_week: this.instance().day_of_week || 0,
      time: this.instance().time || "",
      student_id: this.instance().student_id,
    };
  }

  id(){ return this.instance().id }
  day_of_week(){ return this.instance().day_of_week }
  time(){ return this.instance().time }
  student_id(){ return this.instance().student_id }

  static tableName(){
    return 'Schedule';
  }

  static byStudent(student_id){
    return this.where('student_id', parseInt(student_id));
  }

  static byDayNumber(dayNumber){
    var result = [];
    var all = this.getAll();
    all.forEach(function(item){
      if(dayNumber == item.day_of_week()){
        result.push(item);
      }
    });

    return result;
  }

  student(){
    return Student.find(this.student_id());
  }
}
