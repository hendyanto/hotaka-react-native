import Schedule from  './Schedule';
import Debt from  './Debt';
import Scheduleable from  '../roles/Scheduleable';
import { todayDate, todayDayOfWeek } from  '../helpers/Date';
export default class TodaySchedule {
  schedule(){
    return Schedule;
  }

  debt(){
    return Debt;
  }

  regulars(){
    var results = [];
    var schedules = this.schedule().byDayNumber(todayDayOfWeek());
    return schedules.map((item) => {
      return new Scheduleable(item);
    });
  }

  payments(){
    var results = [];
    var debts = this.debt().byPaymentDate(todayDate());
    return debts.map((item) => {
      return new Scheduleable(item);
    });
  }

  outstandings(){
    var results = [];
    return this.debt().byDate(todayDate().format("DD-MM-YYYY"));
  }

  get(){
    var regulars = this.regulars();
    var payments = this.payments();
    var outstandings = this.outstandings();
    var stripped_regulars = [];

    regulars.forEach((regular_item) => {
      let matched = false;
      payments.forEach((payment_item) => {
        if(regular_item.studentId() == payment_item.studentId()){
          matched = true;
        }
      });

      if(!matched){
        outstandings.forEach((outstanding_item) => {
          if(
            outstanding_item.getTime() == regular_item.time()
            && outstanding_item.student_id() == regular_item.studentId()
          ){
            regular_item.setDebt(outstanding_item);
          }
        });
        stripped_regulars.push(regular_item);
      }
    });
    return stripped_regulars.concat(payments);
  }
}
