import Realm from 'realm';
import Root from './Root';

export default class Model {
  constructor(item){
    this._instance = item;
  }

  instance(){
    return this._instance || {};
  }

  /*Processed*/
  static tableName(){
    return "";
  }

  static data(){
    return Root;
  }

  static _all(){
    return Model.data().objects(this.tableName());
  }

  static getAll(){
    var results = [];
    var raws = this._all();
    raws.forEach((item) => {
      results.push(new this.prototype.constructor(item));
    });

    return results;
  }

  static count(){
    return this._all().length || 0;
  }

  static find(id=0){
    var items = this._all().filtered('id == $0', id);
    if(items.length > 0){
      return (new this.prototype.constructor(items[0]));
    } else {
      return null;
    }
  }

  static where(key, value){
    var results = [];
    var items = this._all().filtered(`${key} == ${value}`);
    items.forEach((item) => {
      results.push(new this.prototype.constructor(item));
    });

    return results;
  }

  static whereContains(key,value){
    var results = [];
    var items = this._all().filtered(`${key} CONTAINS '${value}'`);
    items.forEach((item) => {
      results.push(new this.prototype.constructor(item));
    });

    return results;
  }

  static getNextId(){
    var all = this._all();
    if(all.length > 0){
      var last = all[all.length-1];
      if(last.id){
        return last.id + 1;
      }
    }

    return 1;
  }

  static _mergeWithNextId(object){
    var mergedWithId = Object.assign({ id: this.getNextId() }, object);
    return mergedWithId;
  }

  static _createFunc(object){
    return this.data().create(this.tableName(), this._mergeWithNextId(object));
  }

  static _updateFunc(object){
    return this.data().create(this.tableName(), object, true);
  }

  static insert(object){
    this.data().write(function(){ this._createFunc(object) }.bind(this));
  }

  static update(object){
    this.data().write(function(){ this._updateFunc(object) }.bind(this));
  }

  static onChange(callback){
    this.data().addListener('change', () => {
      callback();
    });
  }
}
