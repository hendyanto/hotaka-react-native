import Model from './Model';
import Schedule from './Schedule';

export default class Student extends Model {
  constructor(item){
    super(item);
  }

  static tableName(){
    return 'Student';
  }

  name(){
    return this.instance().name;
  }

  grade(){
    return this.instance().grade;
  }

  id(){
    return this.instance().id;
  }

  firstSchedule(){
    var schedules = Schedule.byStudent(this.id());
    if(schedules.length > 0){
      return schedules[0];
    }
    return;
  }


  hash(){
    return {
      id: this.id(),
      name: this.name(),
      grade: this.grade()
    };
  }
}
