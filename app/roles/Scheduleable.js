import Debt from '../models/Debt';
import Schedule from '../models/Schedule';

export default class Scheduleable {
  constructor(item){
    this._instance = item;
  }

  isDebt(){
    if(this._instance instanceof Debt){
      return true;
    }
  }

  time(){
    if(this.isDebt()){
      return this._instance.getPaymentTime();
    }
    return this._instance.time();
  }

  studentId(){
    return this._instance.student_id();
  }

  studentName(){
    return this._instance.student().name();
  }

  setDebt(debt){
    this._debt = debt;
  }

  debt(){
    return this._debt;
  }

  hasDebt(){
    if(typeof this._debt === "undefined"){
      return false;
    }

    return true;
  }
}
