import React from 'react';
import {
  AppRegistry,
  Text,
  Button,
  View,
} from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';
import StudentList from './app/pages/Student/List';
import StudentNew from './app/pages/Student/New';
import StudentEdit from './app/pages/Student/Edit';
import StudentShow from './app/pages/Student/Show';
//
import DebtList from './app/pages/Debt/List';
import DebtNew from './app/pages/Debt/New';
import DebtEdit from './app/pages/Debt/Edit';

import Home from './app/pages/Home';

import ScheduleList from './app/pages/Schedule/List';
import ScheduleNew from './app/pages/Schedule/New';
import ScheduleEdit from './app/pages/Schedule/Edit';

const SimpleApp = TabNavigator({
  Home: { screen: Home },
  StudentList: { screen: StudentList },
});

const MainApp = StackNavigator({
  Root: { screen: SimpleApp },
  StudentNew: { screen: StudentNew },
  StudentEdit: { screen: StudentEdit },
  StudentShow: { screen: StudentShow },
  DebtNew: { screen: DebtNew },
  DebtList: { screen: DebtList },
  DebtEdit: { screen: DebtEdit },
  ScheduleNew: { screen: ScheduleNew },
  ScheduleList: { screen: ScheduleList },
  ScheduleEdit: { screen: ScheduleEdit }
});

AppRegistry.registerComponent('SimpleApp', () => MainApp);
