import { nearestDate, sameDowArray } from '../../app/helpers/Date';
import Moment from 'moment';

describe('nearestDate',()=>{
  it('return nearest date with given week day', () => {
    var date = Moment("03-07-2017","DD-MM-YYYY"); //Monday
    var expected = Moment("10-07-2017","DD-MM-YYYY");
    expect(nearestDate(date,1).isSame(expected)).toEqual(true);
  });
  it('return nearest date with given week day', () => {
    var date = Moment("03-07-2017","DD-MM-YYYY"); //Monday
    var expected = Moment("09-07-2017","DD-MM-YYYY");
    expect(nearestDate(date,0).isSame(expected)).toEqual(true);
  });
  it('return nearest date with given week day', () => {
    var date = Moment("05-07-2017","DD-MM-YYYY"); //Monday
    var expected = Moment("12-07-2017","DD-MM-YYYY");
    expect(nearestDate(date,3).isSame(expected)).toEqual(true);
  });
});

describe('sameDowArray',()=>{
  it('return array containing 2 days before and after given date with same dayofweek', () => {
    var date = Moment("03-07-2017","DD-MM-YYYY"); //Monday
    var results = sameDowArray(date, 2); //get nearest tuesdays
    var expecteds = [
      Moment("20-06-2017","DD-MM-YYYY"),
      Moment("27-06-2017","DD-MM-YYYY"),
      Moment("04-07-2017","DD-MM-YYYY"),
      Moment("11-07-2017","DD-MM-YYYY"),
      Moment("18-07-2017","DD-MM-YYYY"),
    ].map((item) => {
      return item.format("DD-MM-YYYY");
    });

    expect(results.map((item) => {
      return item.format("DD-MM-YYYY");
    })).toEqual(expecteds);
  });
});
