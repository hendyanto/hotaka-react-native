import TodaySchedule from '../../app/models/TodaySchedule';
import Schedule from '../../app/models/Schedule';
import Debt from '../../app/models/Debt';
import Scheduleable from '../../app/roles/Scheduleable';
import { todayDate, todayDayOfWeek } from  '../../app/helpers/Date';

describe('TodaySchedule', () => {
  var todaySchedule = new TodaySchedule();
  describe('.get', () => {

    var student_1 = new Scheduleable(),
      student_1_diff_sched = new Scheduleable(),
      student_2 = new Scheduleable(),
      student_3 = new Scheduleable(),
      student_4 = new Scheduleable();

    student_1.studentId = jest.fn().mockReturnValue(1);
    student_1.time = jest.fn().mockReturnValue('00:00');

    student_2.studentId = jest.fn().mockReturnValue(2);
    student_2.time = jest.fn().mockReturnValue('00:00');

    student_3.studentId = jest.fn().mockReturnValue(3);
    student_3.time = jest.fn().mockReturnValue('00:00');

    student_4.studentId = jest.fn().mockReturnValue(4);
    student_4.time = jest.fn().mockReturnValue('00:00');

    student_1_diff_sched.studentId = jest.fn().mockReturnValue(1);
    student_1_diff_sched.time = jest.fn().mockReturnValue('99:99');

    test('it combines schedule and debt list',() => {
      todaySchedule.regulars = jest.fn().mockReturnValue([
        student_1,
        student_2,
      ]);
      todaySchedule.payments = jest.fn().mockReturnValue([
        student_3,
        student_4,
      ]);

      var result = todaySchedule.get();
      expect(result.map((i) => { return i.studentId() })).toEqual(expect.arrayContaining([
        1,2,3,4
      ]));
    });
    test('it prioritize payments if it have same student_id',() => {
      todaySchedule.regulars = jest.fn().mockReturnValue([
        student_1,
        student_2,
      ]);
      todaySchedule.payments = jest.fn().mockReturnValue([
        student_1_diff_sched,
        student_2,
      ]);

      var result = todaySchedule.get();
      expect(result.map((i) => { return [i.studentId(), i.time()] })).toEqual(expect.arrayContaining([
        [1, '99:99'],
        [2, '00:00']
      ]));
      expect(result.length).toEqual(2);
    });
  });
  describe('#regulars', () => {
    it("get all today schedules and wrap it in scheduleable class", function(){
      var scheduleInstance = new Schedule();

      var todaySchedule = new TodaySchedule();
      todaySchedule.schedule().byDayNumber = jest.fn().mockReturnValue([scheduleInstance]);

      var result = todaySchedule.regulars();

      expect(todaySchedule.schedule().byDayNumber).toHaveBeenCalledWith(todayDayOfWeek());
      expect(result[0]).toBeInstanceOf(Scheduleable);
    });
  });
  describe('#payments', () => {
    it("get all today debt payments and wrap it in scheduleable class", function(){
      var debtInstance = new Debt();

      var todaySchedule = new TodaySchedule();
      todaySchedule.debt().byPaymentDate = jest.fn().mockReturnValue([debtInstance]);

      var result = todaySchedule.payments();

      expect(todaySchedule.debt().byPaymentDate).toHaveBeenCalledWith(todayDate());
      expect(result[0]).toBeInstanceOf(Scheduleable);
    });
  });
});

