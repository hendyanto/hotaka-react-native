import Model from '../../app/models/Model';
import Realm from 'realm';

class ModelInstance extends Model {
  constructor(){
    super();
    this.test = true;
  }

  static tableName(){
    return "ModelInstance";
  }
}

describe('Model', () => {
  test('.data', () => {
    expect(Model.data()).toBeInstanceOf(Realm);
  });

  test('.tableName', () => {
    expect(Model.tableName()).toEqual("");
  });

  describe("._all",() => {
    test('return objects from Realm', () => {
      var mockReturn = [{fake: "object"}];
      Model.tableName = jest.fn().mockReturnValue("test-tableName");
      Model.data().objects = jest.fn();
      Model.data().objects.mockReturnValue(mockReturn);

      var results = Model._all();
      expect(Model.data().objects).toHaveBeenCalledWith("test-tableName");
      expect(results).toEqual(mockReturn);
    });

    test('it uses child class _tableName attribute', () => {
      var mockReturn = [{fake: "object"}];
      ModelInstance.data().objects = jest.fn();
      ModelInstance.data().objects.mockReturnValue(mockReturn);

      var results = ModelInstance._all();
      expect(ModelInstance.data().objects).toHaveBeenCalledWith("ModelInstance");
      expect(results).toEqual(mockReturn);
    });
  });

  describe('.getAll', () => {
    test('create new instance of current class for every result', () => {
      Model._all = jest.fn();
      Model._all.mockReturnValue([{fake: "object"}]);

      var results = ModelInstance.getAll();

      expect(results[0]).toBeInstanceOf(ModelInstance);
      expect(results[0].test).toEqual(true);
    });
  });

  describe('.count', () => {
    test("return length when available", () => {
      Model._all = jest.fn().mockReturnValue({ length: 99 });
      expect(Model.count()).toEqual(99);
    });
    test("return 0 when length is not available", () => {
      Model._all = jest.fn().mockReturnValue({});
      expect(Model.count()).toEqual(0);
    });
  });

  describe('.find', () => {
    test("return null when item is not available", () => {
      Model._all().filtered = jest.fn().mockReturnValue([]);
      expect(Model.find(1)).toEqual(null);
      expect(Model._all().filtered).toHaveBeenCalledWith("id == $0", 1);
    });
    test("return first of array when item is available", () => {
      Model._all().filtered = jest.fn().mockReturnValue(["first","second"]);
      expect(ModelInstance.find(1)).toBeInstanceOf(ModelInstance);
      expect(Model._all().filtered).toHaveBeenCalledWith("id == $0", 1);
    });
  });

  describe('.where', () => {
    test("return anything coming from filter", () => {
      Model._all().filtered = jest.fn().mockReturnValue(["first","second"]);
      var result = Model.where('key','value');
      expect(Model._all().filtered).toHaveBeenCalledWith("key == value");
      expect(result[0]).toBeInstanceOf(Model);
    });
  });

  describe('.getNextId', () => {
    test("return last id + 1", () => {
      Model._all = jest.fn().mockReturnValue([{id: 1},{id: 2}]);
      expect(Model.getNextId()).toEqual(3);
    });
    test("return 1 when no data present", () => {
      Model._all = jest.fn().mockReturnValue([]);
      expect(Model.getNextId()).toEqual(1);
    });
  });

  describe("._createFunc", () => {
    test("it calls data create with object + latest id", () => {
      var object = { foo: "bar" };
      var object_with_id = { id: 9, foo: "bar" };
      Model._mergeWithNextId = jest.fn().mockReturnValue(object_with_id);
      Model.data().create = jest.fn();
      Model.tableName = jest.fn().mockReturnValue("tableName");
      Model._createFunc(object);

      expect(Model._mergeWithNextId).toHaveBeenCalledWith(object);
      expect(Model.data().create).toHaveBeenCalledWith("tableName", object_with_id);
    });
  });

  describe(".insert",() => {
    test("it pass _createFunc to data write", () => {
      var obj = {foo: "bar"};
      Model._createFunc = jest.fn().mockReturnValue(function(){});

      Model.insert(obj)

      expect(Model._createFunc).toHaveBeenCalledWith(obj);
    });
  });

  describe(".update",() => {
    test("it pass _updateFunc to data write", () => {
      var obj = {foo: "bar"};
      Model._updateFunc = jest.fn().mockReturnValue(function(){});

      Model.update(obj)

      expect(Model._updateFunc).toHaveBeenCalledWith(obj);
    });
  });
});
